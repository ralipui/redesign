module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'salmon': '#F09196'
      }, fontFamily: {
        'montserrat': ['Montserrat']
      }
    },
  },
  plugins: [],
}