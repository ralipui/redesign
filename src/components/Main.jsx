import React from "react";
import logo from "../img/Dela-Logo.png";
import dela_cv from "../docs/Dela_CV.pdf";
// import illustration from "../img/Error_Page.png";

// const Main = () => {
//
//
//     return(
//
//         <div className="flex flex-col font-montserrat text-gray-700 mt-24 xl:mt-16 2xl:mt-24">
//             <div className="flex flex-col text-center mb-8">
//                 <span className="text-5xl font-thin text-salmon mb-4 md:mb-4 font-semibold">Oops!</span>
//                 <span className="text-lg 2xl:text-3xl font-medium">Where is the page?</span>
//             </div>
//             {/*<div className="flex justify-center mb-10 2xl:mb-16">*/}
//             {/*    /!*<img src={illustration} alt="design" className="w-56 xl:w-64"/>*!/*/}
//             {/*</div>*/}
//             <div className="flex flex-col text-center text-sm xl:text-lg tracking-wide">
//                 <span>Currently <span className="text-salmon font-semibold">redesigning</span> my website.</span>
//                 <span>Sorry for the inconvenience</span>
//             </div>
//         </div>
//
//     )
// }

const BigScreen = () => {

    return (
        <div className="flex font-montserrat text-gray-700 ">
                <div className="flex flex-col mx-auto min-h-screen justify-center">
                    <div className="flex flex-col text-center mb-24 lg:mb-14">
                        <div className="flex justify-center mb-8">
                            <img src={logo} alt="logo" className="w-24 xl:w-28 2xl:w-32"/>
                        </div>
                        <span className="text-4xl xl:text-5xl 2xl:text-6xl font-thin text-black mb-4 md:mb-6">Coming Soon!</span>
                        {/*<span className="text-xl 2xl:text-3xl font-medium mb-8">Where is the page?</span>*/}
                        <div className="flex flex-col text-sm 2xl:text-lg tracking-wide">
                            <span>Currently redesigning my website. Sorry for the inconvenience</span>
                        </div>

                        <div className="flex flex-col text-sm 2xl:text-sm tracking-wide pt-5">
                            <a href={dela_cv} rel="noreferrer" target="_blank" className="underline decoration-1 decoration-salmon cursor-pointer font-bold"><span className="text-salmon">Click to view resume</span></a>
                        </div>
                    </div>
                </div>
                {/*<div className="">*/}
                {/*    <img src={illustration} alt="design" className="w-3/4"/>*/}
                {/*</div>*/}

        </div>
    )
}

// export default Main;
export {BigScreen}