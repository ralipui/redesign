import React from "react";
import Nav from "./Nav";
import Main from "./Main";
import {BigScreen} from "./Main";
import Footer from "./Footer";


const Redesign = () => {

    // const [width, setWidth] = React.useState(window.innerWidth);
    // const breakpoint = 800;
    // React.useEffect(() => {
    //     const handleResizeWindow = () => setWidth(window.innerWidth);
    //     // subscribe to window resize event "onComponentDidMount"
    //     window.addEventListener("resize", handleResizeWindow);
    //     return () => {
    //         // unsubscribe "onComponentDestroy"
    //         window.removeEventListener("resize", handleResizeWindow);
    //     };
    // }, []);

    // if (width > breakpoint) {
        return (
            <section className="overflow-y-hidden">
                {/*<Nav/>*/}
                <BigScreen/>
                <Footer/>
            </section>
        );
    // }
    // return (
    //     <section className="overflow-y-hidden">
    //         {/*<Nav/>*/}
    //         <Main/>
    //     </section>
    // );


}

export default Redesign;