import React from "react";
import Socials from "./Socials";



const Footer = () => {

    return (
    <footer className="w-full">

        <div className="flex flex-col absolute bottom-4 text-center font-montserrat text-gray-400 w-full">
            <div className="flex justify-center pb-8">
                <Socials/>
            </div>
            <div className="flex flex-col justify-center lg:flex-row lg:gap-x-3 text-xs 2xl:tracking-wide w-full xl:pb-3 2xl:pb-4">
                <span>&copy; 2023 Dela Alipui. All right reserved.</span>
                <span className="h-1 w-1 bg-salmon rounded-full my-auto hidden lg:block"></span>
                <span>Developed by Richard Alipui.</span>

            </div>

        </div>
    </footer>
    )
}


export default Footer;