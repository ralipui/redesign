import React from "react";
import logo from "../img/Dela-2.png";

const Nav = () => {
    return(
        <nav className="w-full">
            <div className="flex justify-center mt-2">
                <div>
                    <img src={logo} alt="logo" className="w-24 2xl:w-full"/>
                </div>
            </div>
        </nav>
    )
}

export default Nav;